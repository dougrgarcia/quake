                       QUAKE 3 ARENA LOG                            

               AUTOR: DOUGLAS RODRIGUES GARCIA                      
               E-MAIL: dougrgarcia92@gmail.com                      

			   
 OBJETIVO: LER O ARQUIVO DE LOG QUE SE ENCONTRA NA RAIZ DO PROJETO,  
 EXTRAIR AS INFORMAÇÕES DE CADA JOGO, E RETORNAR VIA API A 		 
 QUANTIDADE TOTAL DE MORTES, QUAIS JOGADORES ESTAVAM CONECTADOS E     	 	
 E A PONTUAÇÃO DE CADA UM DOS JOGADORES						
 

 PARA EXECUTAR O PROJETO, DEVE-SE BAIXAR O PROJETO PELO O GIT       
 UTILIZANDO O COMANDO ABAIXO:										 
 git clone https://dougrgarcia@bitbucket.org/dougrgarcia/quake.git  
 APÓS BAIXAR O PROJETO IMPORTE NO "SPRING TOOLS SUITE" SEGUINDO O   
 SEGUINTE CAMINHO: "FILE" -> "IMPORT" -> "EXISTING MAVEN PROJECT"	 
 APÓS IMPORTADO INICIE A APLICAÇÃO CLICANDO EM "RUN AS" -> 		 	
 "SPRING BOOT APP"													 

 
 A CHAMADA DA API VIA POSTMAN OU DIRETO PELO NAVEGADOR USANDO A SEGUINTE URL 
 "http://localhost:8080/quaketreearena/games", ESCOLHA O METODO "GET" COLOQUE A URL ABAIXO:						 
 E CLIQUE EM "SEND"	
 
 O ARQUIVO GAMES.LOG DEVE ESTAR NA RAIZ DO PROJETO PARA FUNCIONAR CORRETAMENTE, DO CONTRÁRIO SERA
 RETORNADO O STATUS 404 "NOT FOUND".	
 										 
 O RETORNO SERA UM STATUS 200 "OK", E UMA STRING NO FORMATO JSON PARECIDA COM ESTA:		 										 
    "Game_3": {
    "total_kills": 4,
    "players": [
      "Dono da Bola",
      "Isgalamido",
      "Zeh"
    ],
    "kills": {
      "Dono da Bola": -1,
      "Isgalamido": 1,
      "Zeh": -2
    }
  }						
  

 PARA OS TESTES UNITÁRIOS DEVE IR PELA IDE "SPRING TOOLS SUIT" EM   
 "src/test/java" -> "com.quaketreearena"  					     	 
 CLICAR COM O BOTÃO DIREITO SOBRE "QuaketreearenaApplicationTests"  
 DEPOIS "RUN AS" -> "JUNIT TEST"									 

	




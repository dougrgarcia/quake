package com.quaketreearena.gameutils;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class GameUtils {
	
	//verifica se é um novo jogo
		public static boolean checkInitGame(String value){
			Pattern pattern = Pattern.compile("InitGame");
			Matcher matcher = pattern.matcher(value);
			return matcher.find();
		}
		
		
		//verifica se é uma morte
		public static boolean checkKill(String value){
			Pattern pattern = Pattern.compile("Kill");
			Matcher matcher = pattern.matcher(value);
			return matcher.find();
		}
		
		
		//verifica se é o final do jogo
		public static boolean checkShutdownGame(String value){
			Pattern pattern = Pattern.compile("ShutdownGame");
			Matcher matcher = pattern.matcher(value);
			return matcher.find();
		}
				
		
		//retornando nome do jogador 
		public static  String getClientUserinfoChanged(String value){
			String aux = ""; 
			value = value.replace("n\\", "<initTag>");
			value = value.replace("\\t", "<finalTag>");
			Pattern pattern = Pattern.compile("<initTag>([A - Z]+[^<]+)<finalTag>");
			Matcher matcher = pattern.matcher(value);
			if(matcher.find()){
				aux = matcher.group();
			}
			aux = aux.replace("<initTag>","");
			aux = aux.replace("<finalTag>","");
			return aux;
		}
		
		
		//retorna quem matou
		public static String getKiller(String value){
			String auxDead = "";
			Pattern pattern = Pattern.compile(":([^:]+) killed");
			Matcher matcher = pattern.matcher(value);
			if(matcher.find()){
				auxDead = matcher.group();
			}
			auxDead = auxDead.replace(" killed","");
			auxDead = auxDead.replace(": ","");
			return auxDead.trim();
		}
		
		
		//retorna quem morreu
		public static String getDead(String value){
			String auxDead = "";
			Pattern pattern = Pattern.compile("killed([^k]+)by");
			Matcher matcher = pattern.matcher(value);
			if(matcher.find()){
				auxDead = matcher.group();
			}
			auxDead = auxDead.replace("killed ","");
			auxDead = auxDead.replace(" by","");
			return auxDead;
		}
		
		
		//lendo o arquivo e retornando uma lista
		public static List<String> readFile(String path) throws FileNotFoundException{
			
			List<String> list = new ArrayList<String>();
			Scanner file = new Scanner(new FileReader(path));
			while (file.hasNextLine()) {
				list.add(file.nextLine());
			}
			file.close();
			return list;
		}
}

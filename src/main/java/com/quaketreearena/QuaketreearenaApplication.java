package com.quaketreearena;

import java.io.IOException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QuaketreearenaApplication {

	public static void main(String[] args) throws IOException {
		SpringApplication.run(QuaketreearenaApplication.class, args);
	}
}

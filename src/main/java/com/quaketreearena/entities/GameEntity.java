package com.quaketreearena.entities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GameEntity {
	
    private int total_kills;
    private List<String>players = new ArrayList<String>();
    private Map<String,Integer> kills = new HashMap<String,Integer>();
    

	public int getTotal_kills() {
		return total_kills;
	}

	public void setTotal_kills(int total_kills) {
		this.total_kills = total_kills;
	}

	public List<String> getPlayers() {
		return players;
	}

	public void setPlayers(List<String> player) {
		this.players = player;
	}

	public Map<String, Integer> getKills() {
		return kills;
	}

	public void setKills(Map<String, Integer> kills) {
		this.kills = kills;
	}

	
	
}
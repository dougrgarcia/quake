package com.quaketreearena.controllers;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.quaketreearena.entities.GameEntity;
import com.quaketreearena.gameutils.GameUtils;

@Controller
@RestController
@RequestMapping("/quaketreearena")
public class GameController {
	@RequestMapping(value = "/games", method = RequestMethod.GET)
	public ResponseEntity<HashMap<String, GameEntity>> findGames() throws FileNotFoundException {
		// Declarando variaveis necessáris e atribui o caminho do arquivo de log

		String path = System.getProperty("user.dir") + "\\games.log";

		int gameNumber = 0;
		int totalKills = 0;
		GameEntity game = null;
		LinkedHashMap<String, GameEntity> games = new LinkedHashMap<String, GameEntity>();
		List<String> player = null;
		Map<String, Integer> kill = null;

		try {
			// Percorre as linhas extraidas do arquivo
			for (String line : GameUtils.readFile(path)) {

				// verifica inicio do jogo 
				if (GameUtils.checkInitGame(line)) {
					gameNumber += 1;
					totalKills = 0;
					player = new ArrayList<String>();
					game = new GameEntity();
					kill = new HashMap<String, Integer>();
				}
				//adiciona jogadores ao jogo

				if (!GameUtils.getClientUserinfoChanged(line).isEmpty()
						&& !GameUtils.getClientUserinfoChanged(line).equals("")
						&& GameUtils.getClientUserinfoChanged(line) != null) {
					Boolean existList = false;
					Set<String> kills = kill.keySet();
					for (String i : kills) {
						if (i.equals(GameUtils.getClientUserinfoChanged(line))) {
							existList = true;
						}
					}
					if (existList == false) {
						kill.put(GameUtils.getClientUserinfoChanged(line), 0);
					}
				}

				//calcula as mortes do jogo
				if (GameUtils.checkKill(line)) {
					totalKills += 1;
					if ((GameUtils.getKiller(line).equals("<world>"))
							|| (GameUtils.getKiller(line).equals(GameUtils.getDead(line)))) {
						if (kill.containsKey(GameUtils.getDead(line))) {
							kill.replace(GameUtils.getDead(line), kill.get(GameUtils.getDead(line)) - 1);
						} else {
							kill.put(GameUtils.getDead(line), -1);
						}
					} else {
						if (kill.containsKey(GameUtils.getKiller(line))) {
							kill.replace(GameUtils.getKiller(line), kill.get(GameUtils.getKiller(line)) + 1);
						} else {
							kill.put(GameUtils.getKiller(line), +1);
						}

					}

				}
				//verifica e incrementa as variaveis
				if (GameUtils.checkShutdownGame(line)) {
					game.setTotal_kills(totalKills);
					game.setKills(kill);
					Set<String> kills = kill.keySet();
					for (String i : kills) {
						player.add(i);
					}
					game.setPlayers(player);
					games.put("Game_" + gameNumber, game);

				}

			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);

		}
		return new ResponseEntity<HashMap<String, GameEntity>>(games, HttpStatus.OK);
	}

}
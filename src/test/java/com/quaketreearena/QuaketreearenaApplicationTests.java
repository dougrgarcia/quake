package com.quaketreearena;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.quaketreearena.gameutils.GameUtils;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebAppConfiguration
@RunWith(SpringRunner.class)
@SpringBootTest
public class QuaketreearenaApplicationTests {
	private MockMvc mockMvc;
	GameUtils mock = Mockito.mock(GameUtils.class);
	
	@Autowired WebApplicationContext wac; 
    @Autowired MockHttpServletRequest request;
    
	
	@Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
	
	//testando chamada da api
	@Test
    public void testFoundFindGames() throws Exception {
        mockMvc.perform(get("/quaketreearena/games"))
        .andExpect(status().isOk());
        
    }
	
	@Test
    public void testNotFoundFindGames() throws Exception {
        mockMvc.perform(get("/quaketreearena/games2"))
        .andExpect(status().isNotFound());
    }
	
	//testando metodos usados
	@Test
	public void testCheckGetInit()throws Exception {
		assertTrue(GameUtils.checkInitGame("  0:00 InitGame: sv_floodProtect"));
		assertFalse(GameUtils.checkInitGame("20:37 ShutdownGame:"));
	}
	
	
	@Test
	public void testCheckShutdownGame()throws Exception {
		assertTrue(GameUtils.checkShutdownGame("20:37 ShutdownGame:"));
		assertFalse(GameUtils.checkShutdownGame("  0:00 InitGame: sv_floodProtect"));
	}
	
	
	@Test
	public void testCheckKill()throws Exception {
		assertTrue(GameUtils.checkKill(" 20:54 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT"));
		assertFalse(GameUtils.checkKill("  0:00 InitGame: sv_floodProtect"));
	}
	
	
	@Test
	public void testGetClientUserinfoChanged()throws Exception {
		assertTrue(GameUtils.getClientUserinfoChanged("  1:06 ClientUserinfoChanged: 4 n\\Zeh\\t\\0\\model\\s").equals("Zeh"));
		assertTrue(GameUtils.getClientUserinfoChanged("  0:00 InitGame: sv_floodProtect").equals(""));
	}
	
	@Test
	public void testGetKiller()throws Exception {
		assertTrue(GameUtils.getKiller(" 22:06 Kill: 2 3 7: Isgalamido killed Mocinha by MOD_ROCKET_SPLASH").equals("Isgalamido"));
		assertTrue(GameUtils.getKiller(" 23:06 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT").equals("<world>"));
	}
		
	@Test
	public void testGetDead()throws Exception {
		assertTrue(GameUtils.getDead(" 23:06 Kill: 1022 2 22: <world> killed Isgalamido by MOD_TRIGGER_HURT").equals("Isgalamido"));
		assertTrue(GameUtils.getDead("  0:00 InitGame: sv_floodProtect").equals(""));
	}
	
	
	
}
